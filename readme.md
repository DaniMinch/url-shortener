# Spring-based URL Shortener

Current version is prototype in fact.
Just to check that it is somehow working.

## Prerequisites

Install MongoDB and have it running

## ToDo

- ID Dispenser Improvements

Key (sequence number) is atomically retrieved from sequence table on each insertion.
That means 2 queries per one request what may be an issue.

Plus it is required or to add an index to `key` field (better for business logic) or use key as `_id` if we are trying to spare disks

- Decide what to do with invalid URLs

If incorrect URL is given it is parsed during input
Default annotation does not accept cases like `google.com`

If it not an option it is required to weaken validation.
Other option is to add a handler during redirect processing.

- Homogenous DTO

Use interfaces for definition of Base objects?
Check how to handle repository constraints to define output of findBy methods(optional/List/Object)


- Concurrency

During design it was planned to make possible application scaling and to support multiple nodes.
But it wasn't really tested so there might be some empty places. 

- Help Page is not done

## Unknown point

- using of context to retrieve base URL

I didn't investigate how it works in detail so maybe it does not guarantee stable output

- username are case sensitive

In fact I just forgot about it, but let it be feature  

- validation of input params

I wasn't able to find out what is the most convenient/correct way define default parameters and validate them.
So I have created very straightforward validation annotation and leave standard output.
Plus I wasn't able to find a way to declare validation annotations only for ShortUrlRequest Class.

- testing stuff

Cannot say that tests are implemented using some fixed approach because I don't know much about it.
So there only some basic stuff covering obvious requirements.
And they are not made clean because cleaning up requires time to think.


- POST redirects

If there is a need to handle POST redirects then should be found out exclude mechanism for other mapped path

- `AccountId` JSON property

In a description JSON property started from capital character.
Simple @RequestMapping do not handle such case.
If it is necessary it should be fixed 