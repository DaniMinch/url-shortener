package com.daniminch.ftry.urlshortener.auth.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class PasswordGenerationServiceTest {

    @TestConfiguration
    static class TestContextConfiguration{

        @Bean
        public PasswordGenerationService service(){
            return new PasswordGenerationServiceImpl();
        }

    }

    @Autowired
    private PasswordGenerationService service;

    @Test
    public void testIDExists() {

        assertNotNull(service.generatePassword());

    }

}