package com.daniminch.ftry.urlshortener.auth.services;

import com.daniminch.ftry.urlshortener.auth.daos.UserRepository;
import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountResponse;
import com.daniminch.ftry.urlshortener.auth.dtos.UserDocument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class UrlShortenerUserDetailServiceTest {

    @TestConfiguration
    static class TestContextConfiguration{

        @Bean
        public UrlShortenerUserDetailService service(){
            return new MongoUserDetailsServiceImpl();
        }

    }

    @MockBean
    private UserRepository repository;

    @MockBean
    private PasswordGenerationService passGen;

    @Autowired
    private UrlShortenerUserDetailService service;

    private static final String USERNAME = "testuser";
    private static final UserDocument USERDOC = new UserDocument(USERNAME, "password");

    @Test
    public void testUserNotFoundExceptionThrown() {

        given(repository.findByUsername(USERNAME)).willReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> service.loadUserByUsername(USERNAME));
    }

    @Test
    public void testUserFoundAndReturned() {

        given(repository.findByUsername(USERNAME)).willReturn(Optional.of(USERDOC));

        UserDetails user = service.loadUserByUsername(USERNAME);

        assertNotNull(user);

        assertEquals(USERNAME, user.getUsername());
    }

    @Test
    public void testUserRegisteredSuccessfully() {

        // To register user it shouldn't Exist
        given(repository.findByUsername(USERNAME)).willReturn(Optional.empty());

        given(passGen.generatePassword()).willReturn("ThePassword");

        UserAccountResponse response = service.registerAccountByName(USERNAME);

        assertNotNull(response);

        assertTrue(response.isSuccess());
        assertEquals("Your account is opened", response.getDescription());
        assertNotNull(response.getPassword());
    }

    @Test
    public void testRegisterAlreadyGivenUser() {

        // To register user it should exist
        given(repository.findByUsername(USERNAME)).willReturn(Optional.of(USERDOC));

        UserAccountResponse response = service.registerAccountByName(USERNAME);

        assertNotNull(response);

        assertFalse(response.isSuccess());
        assertEquals("Account with that ID already exists", response.getDescription());
        assertNull(response.getPassword());
    }


}