package com.daniminch.ftry.urlshortener.auth.dtos;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
public class UserAccountRequestTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testRequestValidationPositiveCases() {

        UserAccountRequest request = new UserAccountRequest();
        request.setAccountId("someaccount");

        Set<ConstraintViolation<UserAccountRequest>> violations = validator.validate(request);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testRequestValidationViolation() {

        UserAccountRequest request = new UserAccountRequest();

        Set<ConstraintViolation<UserAccountRequest>> violations = validator.validate(request);
        assertFalse(violations.isEmpty());

        request.setAccountId(" ");

        violations = validator.validate(request);
        assertFalse(violations.isEmpty());
    }

}