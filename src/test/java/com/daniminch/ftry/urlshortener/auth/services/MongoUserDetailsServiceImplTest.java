package com.daniminch.ftry.urlshortener.auth.services;

import com.daniminch.ftry.urlshortener.auth.daos.UserRepository;
import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountResponse;
import com.daniminch.ftry.urlshortener.auth.dtos.UserDocument;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class MongoUserDetailsServiceImplTest {

    @Autowired
    private UserRepository repository;

    @Autowired
    private MongoUserDetailsServiceImpl service;

    @MockBean
    private PasswordGenerationService passGen;


    private static final String USERNAME = "testuser";
    private static final String USERPASS = "password";
    private static final String USERPASSCRYPT = "$2a$10$A7OPXznZUJff7xZ9qBjQDOspq.JC1gnfBSh9I8XBv7wwpJ3hcz/nW";

    private static final UserDocument USERDOC = new UserDocument(USERNAME, "password");

    private UserDocument saveUser(String username, String password){
        return repository.save(new UserDocument(username, password));
    }

    @Test
    public void testUserRegistered(){
        given(passGen.generatePassword()).willReturn(USERPASS);

        // Clean record if it is needed
        repository.findByUsername(USERNAME).ifPresent(userDocument -> repository.deleteById(userDocument.getId()));

        long initCount = repository.findAll().size();

        UserAccountResponse resp = service.registerAccountByName(USERNAME);

        // Check response
        assertTrue(resp.isSuccess());
        assertEquals(USERPASS, resp.getPassword());

        // Check database
        assertEquals(initCount + 1, repository.findAll().size());
        Optional<UserDocument> doc = repository.findByUsername(USERNAME);

        assertTrue(doc.isPresent());

        assertTrue(new BCryptPasswordEncoder().matches(USERPASS, doc.get().getPassword()), "Passwords are not match");

    }

    @Test
    public void testUserNotRegistered(){

        //mock generator on different value just to see if Object will be updated
        given(passGen.generatePassword()).willReturn("Not correct password");

        // Clean record if it is needed
        Optional<UserDocument> doc = repository.findByUsername(USERNAME);
        if (!doc.isPresent()){
            System.out.println("Creating new");
            repository.save(new UserDocument(USERNAME, USERPASSCRYPT));
        }
        long initCount = repository.findAll().size();

        UserAccountResponse resp = service.registerAccountByName(USERNAME);

        // Check response
        assertFalse(resp.isSuccess());

        // Check database
        assertEquals(initCount, repository.findAll().size());
        doc = repository.findByUsername(USERNAME);

        // Make Checks on Old Object
        assertTrue(doc.isPresent());
        assertTrue(new BCryptPasswordEncoder().matches(USERPASS, doc.get().getPassword()), "Passwords are not match");

    }

    @After
    public void cleanUp() {
        repository.findByUsername(USERNAME).ifPresent(userDocument -> repository.deleteById(userDocument.getId()));
    }

}