package com.daniminch.ftry.urlshortener.auth.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class PasswordGenerationServiceImplTest {

    private final PasswordGenerationServiceImpl srv = new PasswordGenerationServiceImpl();

    @Test
    public void checkPasswordRestrictions(){

        String pass = srv.generatePassword();

        assertThat(pass.length())
            .withFailMessage("Password is to short.%n Expected: %d. Actual: %d", 10, pass.length())
            .isGreaterThanOrEqualTo(8);

        int letter = 0;
        int num = 0;


        char[] ch = pass.toCharArray();
        for(int i = 0; i < pass.length(); i++) {
            if(Character.isLetter(ch[i])){
                letter++;
            }
            else if(Character.isDigit(ch[i])){
                num++;
            }
        }
        assertThat(num)
            .withFailMessage("Not enough number characters.%n Expected: %d. Actual: %d", 1, num)
            .isGreaterThanOrEqualTo(1);

        assertThat(letter)
            .withFailMessage("Not enough letter characters.%n Expected: %d. Actual: %d", 1, letter)
            .isGreaterThanOrEqualTo(1);

        assertThat(pass.length())
            .withFailMessage("Password <%s> contains not only alphanumeric characters", pass)
            .isGreaterThanOrEqualTo(num + letter);


    }


}