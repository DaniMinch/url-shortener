package com.daniminch.ftry.urlshortener.shortening.dtos;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
public class SourceUrlTest {

    final static String validURL = "http://stackoverflow.com/questions/1567929/website-safe-dataaccess-architecture-question?rq=1";

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void transformDocumentToRequest() {

        SourceUrlDocument document = new SourceUrlDocument();
        document.setUrl(validURL);
        document.setRedirectType(301);

        SourceUrlRequest request = new SourceUrlRequest(document);

        assertEquals(validURL, request.getUrl());
        assertEquals(301, request.getRedirectType());
    }

    @Test
    public void transformRequestToDocument() {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(301);

        SourceUrlDocument document = new SourceUrlDocument(request);

        assertEquals(validURL, document.getUrl());
        assertEquals(301, document.getRedirectType());
    }

    public void testDefaultRedirectType() {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        
        assertEquals(request.getRedirectType(), 302);

        Set<ConstraintViolation<SourceUrlRequest>> violations = validator.validate(request);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testRequestValidationPositiveCases() {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(302);

        Set<ConstraintViolation<SourceUrlRequest>> violations = validator.validate(request);
        assertTrue(violations.isEmpty());

        request.setRedirectType(301);
        violations = validator.validate(request);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testRequestRedirectViolation() {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(200);

        Set<ConstraintViolation<SourceUrlRequest>> violations = validator.validate(request);
        assertFalse(violations.isEmpty());
        
    }

    @Test
    public void testRequestUrlViolation() {

        SourceUrlRequest request = new SourceUrlRequest();

        Set<ConstraintViolation<SourceUrlRequest>> violations = validator.validate(request);
        assertFalse(violations.isEmpty());


        request.setUrl(" ");

        violations = validator.validate(request);
        assertFalse(violations.isEmpty());

        request.setUrl("google.com");

        violations = validator.validate(request);
        assertFalse(violations.isEmpty());

        request.setUrl("Not an URL");

        violations = validator.validate(request);
        assertFalse(violations.isEmpty());
    }
}