package com.daniminch.ftry.urlshortener.shortening.services;

import com.daniminch.ftry.urlshortener.shortening.daos.UrlRepository;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlDocument;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class UrlShorteningServiceTest {

    @TestConfiguration
    static class TestContextConfiguration {

        @Bean
        public UrlShorteningService shortService() {
            return new UrlShorteningServiceImpl();
        }
    }

    @Autowired
    UrlShorteningService service;

    @MockBean
    private IdConverterService idConverter;

    @MockBean
    private UrlRepository repo;

    @MockBean
    private IdDispenserService idDispenser;

    final static String validURL = "http://stackoverflow.com/questions/1567929/website-safe-dataaccess-architecture-question?rq=1";

    @Test
    public void testKeyChangedInDatabase() {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);

        final long initialKey = 2L;
        final long changedKey = 3L;
        final String initialCode = "b";
        final String changedCode = "c";

        given(idDispenser.getNextID()).willReturn(initialKey);
        given(idConverter.encode(initialKey)).willReturn(initialCode);
        given(idConverter.encode(changedKey)).willReturn(changedCode);

        // Changing key from initial to changed
        given(repo.save(any())).willAnswer(invocation -> {
            SourceUrlDocument doc = invocation.getArgument(0);
            if (doc.getKey() == initialKey) {
                doc.setKey(changedKey);
            }
            return doc;
        });

        assertEquals(service.saveAndGetKey(request), changedCode);

    }

    @Test
    public void testKeyNotFoundInDBReturnEmptyOpt() {

        final long initialKey = 2L;

        given(idConverter.decode(any())).willReturn(initialKey);
        given(repo.findByKey(anyLong())).willReturn(null);

        assertFalse(service.getSourceUrl("123").isPresent());
    }

    @Test
    public void testKeyFoundReturn() {

        final long initialKey = 2L;

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);

        SourceUrlDocument document = new SourceUrlDocument();
        document.setUrl(request.getUrl());
        document.setRedirectType(request.getRedirectType());
        document.setKey(initialKey);


        given(idConverter.decode(any())).willReturn(initialKey);
        given(repo.findByKey(anyLong())).willReturn(document);

        Optional<SourceUrlRequest> result = service.getSourceUrl("123");
        assertTrue(result.isPresent());
        assertEquals(result.get().getUrl(), request.getUrl());
        assertEquals(result.get().getRedirectType(), request.getRedirectType());
    }
}