package com.daniminch.ftry.urlshortener.shortening.controllers;

import com.daniminch.ftry.urlshortener.auth.services.UrlShortenerUserDetailService;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlRequest;
import com.daniminch.ftry.urlshortener.shortening.services.UrlShorteningService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UrlShortenerShorteningController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UrlShortenerShorteningControllerTest {

    @MockBean
    private UrlShorteningService repo;

    @MockBean
    private UrlShortenerUserDetailService userSrv;

    @Autowired
    private MockMvc mockMvc;

    final static String validURL = "http://stackoverflow.com/questions/1567929/website-safe-dataaccess-architecture-question?rq=1";

    private String writeJSON(Object anObject) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(anObject);
    }

    @Test
    public void testRegisterSuccessResponse() throws Exception {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);

        given(repo.saveAndGetKey(any())).willReturn("asd");

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .accept(MediaType.APPLICATION_JSON)
                .content(writeJSON(request)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(print());
    }

    @Test
    public void testRegisterInValidDataFailResponse() throws Exception {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(305);

        given(repo.saveAndGetKey(any())).willReturn("asd");

        mockMvc.perform(MockMvcRequestBuilders.post("/register")
            .accept(MediaType.APPLICATION_JSON)
            .content(writeJSON(request)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andDo(print());
    }

    @Test
    public void testRedirectSuccess301() throws Exception {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(301);

        given(repo.getSourceUrl(any())).willReturn(Optional.of(request));


        mockMvc.perform(MockMvcRequestBuilders.get("/{shortKey}", "asd")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMovedPermanently())
                .andDo(print());
    }

    @Test
    public void testRedirectSuccess302() throws Exception {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(302);

        given(repo.getSourceUrl(any())).willReturn(Optional.of(request));


        mockMvc.perform(MockMvcRequestBuilders.get("/{shortKey}", "asd")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andDo(print());
    }

    @Test
    public void testRedirectPostNotAllowed() throws Exception {

        SourceUrlRequest request = new SourceUrlRequest();
        request.setUrl(validURL);
        request.setRedirectType(302);

        given(repo.getSourceUrl(any())).willReturn(Optional.of(request));


        mockMvc.perform(MockMvcRequestBuilders.post("/{shortKey}", "asd")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed())
                .andDo(print());
    }

    @Test
    public void testRedirectNoSuchURL() throws Exception {


        given(repo.getSourceUrl(any())).willReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/{shortKey}", "asd")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());
    }
}