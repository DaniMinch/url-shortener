package com.daniminch.ftry.urlshortener.shortening.services;

import com.daniminch.ftry.urlshortener.utilities.services.SequenceGeneratorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class IdDispenserServiceTest {

    @TestConfiguration
    static class TestContextConfiguration {

        @Bean
        public IdDispenserService dispenserService() {
            return new SeqBasedIdDispenserServiceImpl();
        }
    }

    @Autowired
    private IdDispenserService service;

    @MockBean
    private SequenceGeneratorService gen;

    final private AtomicLong mockInc = new AtomicLong();

    @Test
    public void testIDSequential() {

        given(gen.getSequenceNumber(SeqBasedIdDispenserServiceImpl.URL_SEQUENCE_NAME))
            .willAnswer(invocation -> mockInc.incrementAndGet());

        long id = service.getNextID();
        long followingID = service.getNextID();
        assertEquals(id + 1, followingID);

        id = followingID;
        followingID = service.getNextID();

        assertEquals(id + 1, followingID);
    }

}