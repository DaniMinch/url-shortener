package com.daniminch.ftry.urlshortener;

import com.daniminch.ftry.urlshortener.auth.daos.UserRepository;
import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountRequest;
import com.daniminch.ftry.urlshortener.shortening.daos.UrlRepository;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlRequest;
import io.restassured.RestAssured;
import io.restassured.config.RedirectConfig;
import io.restassured.response.ValidatableResponse;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class UrlShortenerApplicationTest {

    private static final String USER_NAME = "testuser";
    private static final String USER_PASS = "password";
    private static final String USER_PASS_CRYPT = "$2a$10$A7OPXznZUJff7xZ9qBjQDOspq.JC1gnfBSh9I8XBv7wwpJ3hcz/nW";
    private static final String USER_BASE64 = "dGVzdHVzZXI6cGFzc3dvcmQ=";

    private final static String VALID_URL = "http://stackoverflow.com/questions/1567929/website-safe-dataaccess-architecture-question?rq=1";
    private final static String INVALID_URL = "google.com";
    private final static String NAURL = "Not an URL";


    @Autowired
    UrlRepository urlRepo;

    @Autowired
    UserRepository userRepo;

    @LocalServerPort
    private int port;

    /**
     * Before each Test we setup port and explicitly clean up necessary objects
     */
    @Before
    public void setUp() {
        // Clean User
        RestAssured.port = port;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        RestAssured.config = config().redirect(RedirectConfig.redirectConfig().followRedirects(false));
        userRepo.findByUsername(USER_NAME).ifPresent(userDocument -> userRepo.deleteById(userDocument.getId()));
    }

    /**
     * CleanUp all Junk when test is complete
     */
    @After
    public void cleanUp() {
        // Clean User
        userRepo.findByUsername(USER_NAME).ifPresent(userDocument -> userRepo.deleteById(userDocument.getId()));
    }

     /**
     * Wrapper for registration account with name {@code USER_NAME}
     * @return response object
     */
    private ValidatableResponse registerTestAccount() {
        UserAccountRequest req = new UserAccountRequest();
        req.setAccountId(USER_NAME);

        return given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
            .when()
                .post("/account")
            .then();
    }

    /**
     * Wrapper for account creation query from which
     * password is extracted and transformed to BasicAuth header
     *
     * @return headers with BasicAuth Token
     */
    private HttpHeaders createAccAndGetBasicAuthHeaders() {
        // Call same query twice
        ValidatableResponse resp = registerTestAccount();

        HttpHeaders headers = new HttpHeaders();
        String pass = resp.extract().path("password");
        headers.setBasicAuth(USER_NAME, pass);
        return headers;
    }

    /**
     * Simplified password validation
     *
     * @param pass password for testing
     */
    private void testPassword(String pass) {

        Assertions.assertThat(pass.length())
                .withFailMessage("Password is to short.%n Expected: %d. Actual: %d", 10, pass.length())
                .isGreaterThanOrEqualTo(8);

        int alphanum = 0;
        char[] ch = pass.toCharArray();
        for(int i = 0; i < pass.length(); i++) {
            if(Character.isAlphabetic(ch[i]) || Character.isDigit(ch[i])){
                alphanum++;
            }
        }

        Assertions.assertThat(pass.length())
                .withFailMessage("Password <%s> contains not only alphanumeric characters", pass)
                .isGreaterThanOrEqualTo(alphanum);
    }

    @Test
    public void testAccountCreationSuccess() {

        long userCount = userRepo.count();

        ValidatableResponse resp = registerTestAccount();

        resp
            .log().all()
            .statusCode(HttpStatus.OK.value())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body("description", equalTo("Your account is opened"))
            .body("success", equalTo(true))
            .body("password",notNullValue());

        String pass = resp.extract().path("password");

        testPassword(pass);

        assertEquals(userCount + 1, userRepo.count());
        //                .header(new HttpHeaders().setBasicAuth(USER_NAME, password))
    }

    @Test
    public void testAccountCreationAlreadyExist() {

        long userCount = userRepo.count();

        // Preliminary register account to create duplicate
        registerTestAccount();

        //when
        ValidatableResponse resp = registerTestAccount();

        // then
        resp
            .log().all()
            .statusCode(HttpStatus.OK.value())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body("description", equalTo("Account with that ID already exists"))
            .body("success", equalTo(false));

        assertEquals(userCount + 1, userRepo.count());

    }

    // TODO add Negative checks


    @Test
    public void testRegisterValidURLWithDefaultRT() {

        SourceUrlRequest req = new SourceUrlRequest();
        req.setUrl(VALID_URL);

        long urlCount = urlRepo.count();

        // when
        ValidatableResponse resp = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .headers(createAccAndGetBasicAuthHeaders())
                .log().all()
            .when()
                .post("/register")
            .then();

        // then
        resp
            .log().all()
            .statusCode(HttpStatus.OK.value())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body("shortUrl", notNullValue());

        assertEquals(urlCount + 1, urlRepo.count());
    }

    @Test
    public void testFailRegistrationAuth() {

        SourceUrlRequest req = new SourceUrlRequest();
        req.setUrl(VALID_URL);

        long urlCount = urlRepo.count();

        // when
        ValidatableResponse resp = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                // Auth headers removed
                //.headers(createAccAndGetBasicAuthHeaders())
                .body(req)
                .log().all()
            .when()
                .post("/register")
            .then();

        // then
        resp
            .log().all()
            .statusCode(HttpStatus.UNAUTHORIZED.value())
            .contentType(MediaType.APPLICATION_JSON_VALUE);

        assertEquals(urlCount, urlRepo.count());
    }

    @Test
    public void testValidRedirect302() {

        SourceUrlRequest req = new SourceUrlRequest();
        req.setUrl(VALID_URL);

        // register result and extract it
        ValidatableResponse resp = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .headers(createAccAndGetBasicAuthHeaders())
                .body(req)
            .when()
                .post("/register")
            .then();

        String shortURL = resp.extract().path("shortUrl");

        // when
        given()
            .baseUri(shortURL)
        .when()
            .get()
        .then()
            .log().all()
            .statusCode(HttpStatus.FOUND.value())
            .header("Location", VALID_URL);
    }

    @Test
    public void testValidRedirect301() {

        SourceUrlRequest req = new SourceUrlRequest();
        req.setUrl(VALID_URL);
        req.setRedirectType(301);

        // register result and extract it
        ValidatableResponse resp = given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .headers(createAccAndGetBasicAuthHeaders())
                .body(req)
            .when()
                .post("/register")
            .then()
                .log().all();

        String shortURL = resp.extract().path("shortUrl");

        // when
        given()
            .baseUri(shortURL)
        .when()
            .get()
        .then()
            .log().all()
            .statusCode(HttpStatus.MOVED_PERMANENTLY.value())
            .header("Location", VALID_URL);
    }
}