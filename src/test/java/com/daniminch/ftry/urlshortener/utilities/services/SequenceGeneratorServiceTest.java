package com.daniminch.ftry.urlshortener.utilities.services;

import com.daniminch.ftry.urlshortener.utilities.dtos.DatabaseSequence;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class SequenceGeneratorServiceTest {

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private SequenceGeneratorService generator;

    private static final String TEST_SEQ = "TEST_SEQUENCE";
    private static final String TEST_SEQ_2 = "TEST_SEQUENCE_2";

    private long getTestSeqNum(){
        return mongoOperations.findById(TEST_SEQ, DatabaseSequence.class).getSeq();
    }

    @Test
    public void testNewSequenceCreation() {

        mongoOperations.findAndRemove(query(where("_id").is(TEST_SEQ)), DatabaseSequence.class);

        final int collectionSize = mongoOperations.findAll(DatabaseSequence.class).size();

        generator.getSequenceNumber(TEST_SEQ);

        assertThat(mongoOperations.findAll(DatabaseSequence.class).size()).isEqualTo(collectionSize + 1);
        assertThat(this.getTestSeqNum()).isOne();
    }

    @Test
    public void testSequenceIncrement() {

        // creating sequence if it is not created
        generator.getSequenceNumber(TEST_SEQ);

        final long initialNumber = this.getTestSeqNum();

        generator.getSequenceNumber(TEST_SEQ);

        assertThat(this.getTestSeqNum()).isEqualTo(initialNumber + 1);

    }

    @Test
    public void testSequenceNoOverride() {

        // creating sequence if it is not created
        generator.getSequenceNumber(TEST_SEQ);

        final long initialNumber = this.getTestSeqNum();

        generator.getSequenceNumber(TEST_SEQ_2);

        assertThat(this.getTestSeqNum()).isEqualTo(initialNumber);

    }

    @After
    public void cleanUp() {
        mongoOperations.findAndRemove(query(where("_id").is(TEST_SEQ)), DatabaseSequence.class);
        mongoOperations.findAndRemove(query(where("_id").is(TEST_SEQ_2)), DatabaseSequence.class);
    }
}