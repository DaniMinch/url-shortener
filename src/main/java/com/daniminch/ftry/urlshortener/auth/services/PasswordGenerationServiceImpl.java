package com.daniminch.ftry.urlshortener.auth.services;

import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.stereotype.Service;

@Service
public class PasswordGenerationServiceImpl implements PasswordGenerationService {


    private static final int MIN_LOWER = 1;
    private static final int MIN_UPPER = 1;
    private static final int MIN_NUMERIC = 1;

    private static final int PASSWORD_LENGTH = 8;

    /**
     * Generate password which will contain {@code PASSWORD_LENGTH}
     * alphanumeric characters
     * Minimal amount of each type of character is defined by
     * {@code MIN_} constants
     * @return password in plain text
     */
    @Override
    public String generatePassword() {

        PasswordGenerator gen = new PasswordGenerator();
        CharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(MIN_LOWER);

        CharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(MIN_UPPER);

        CharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(MIN_NUMERIC);

        return gen.generatePassword(PASSWORD_LENGTH, lowerCaseRule,
                upperCaseRule, digitRule);
    }
}
