package com.daniminch.ftry.urlshortener.auth.daos;

import com.daniminch.ftry.urlshortener.auth.dtos.UserDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<UserDocument, String> {

    Optional<UserDocument> findByUsername(String username);

}
