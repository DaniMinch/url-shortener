package com.daniminch.ftry.urlshortener.auth.dtos;

import javax.validation.constraints.NotBlank;

public class UserAccountRequest {

    @NotBlank(message = "{val.err.account.is.blank}")
    private String accountId;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "UserAccountRequest{" +
                "AccountId='" + accountId + '\'' +
                '}';
    }
}
