package com.daniminch.ftry.urlshortener.auth.services;

import com.daniminch.ftry.urlshortener.auth.daos.UserRepository;
import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountResponse;
import com.daniminch.ftry.urlshortener.auth.dtos.UserDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class MongoUserDetailsServiceImpl implements UrlShortenerUserDetailService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PasswordGenerationService passGen;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserDocument user = repository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("User is not found"));

        return new User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }


    @Transactional
    public UserAccountResponse registerAccountByName(String accountName) {

        UserAccountResponse response = new UserAccountResponse();

        Optional<UserDocument> userOpt = repository.findByUsername(accountName);
        response.setSuccess(!userOpt.isPresent());

        if(response.isSuccess()) {
            response.setDescription("Your account is opened");

            String password = passGen.generatePassword();

            // respond password in plain text
            response.setPassword(password);

            // but save encrypted version
            repository.save(new UserDocument(accountName, BCrypt.hashpw(password, BCrypt.gensalt())));

        } else {
            response.setDescription("Account with that ID already exists");
        }

        return response;

    }
}
