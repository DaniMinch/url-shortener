package com.daniminch.ftry.urlshortener.auth.services;

import javax.validation.constraints.NotNull;

public interface PasswordGenerationService {

    /**
     * Method Somehow generates password
     * @return any kind of password
     */
    @NotNull String generatePassword();
}
