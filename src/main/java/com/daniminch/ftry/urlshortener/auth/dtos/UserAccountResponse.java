package com.daniminch.ftry.urlshortener.auth.dtos;

import org.hibernate.validator.constraints.Length;

public class UserAccountResponse {

    boolean success;

    String description;

    @Length(min = 8)
    String password;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
