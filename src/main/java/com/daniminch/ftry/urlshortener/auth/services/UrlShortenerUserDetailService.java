package com.daniminch.ftry.urlshortener.auth.services;

import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UrlShortenerUserDetailService extends UserDetailsService {

    /**
     * Registers Accounts with given name if it is not exists
     *
     * @param accountName name of account for registration
     * @return {@code UserAccountResponse} with result
     */
    UserAccountResponse registerAccountByName(String accountName);
}
