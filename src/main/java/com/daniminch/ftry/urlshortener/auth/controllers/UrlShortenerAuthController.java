package com.daniminch.ftry.urlshortener.auth.controllers;

import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountRequest;
import com.daniminch.ftry.urlshortener.auth.dtos.UserAccountResponse;
import com.daniminch.ftry.urlshortener.auth.services.UrlShortenerUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping
public class UrlShortenerAuthController {

    @Autowired
    private UrlShortenerUserDetailService userService;

    @PostMapping(
            value = "/account",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserAccountResponse> createAccount(@Valid @RequestBody UserAccountRequest accountRequest) {

        return ResponseEntity.ok(userService.registerAccountByName(accountRequest.getAccountId()));
    }
}
