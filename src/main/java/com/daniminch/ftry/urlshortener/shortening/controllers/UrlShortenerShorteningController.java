package com.daniminch.ftry.urlshortener.shortening.controllers;

import com.daniminch.ftry.urlshortener.shortening.dtos.ShortUrlResponse;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlBaseDto;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlRequest;
import com.daniminch.ftry.urlshortener.shortening.services.UrlShorteningService;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping
public class UrlShortenerShorteningController {

    @Autowired
    private UrlShorteningService urlService;

    @URL
    private String getCurrentBasePath(){

        return ServletUriComponentsBuilder.fromCurrentContextPath().toUriString();
    }

    @PostMapping(
        value = "/register",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShortUrlResponse> shortenUrl(@Valid @RequestBody SourceUrlRequest sourceUrlRequest) {


        final String key = urlService.saveAndGetKey(sourceUrlRequest);

        // 200 is used because we return values in body not in header
        return ResponseEntity.ok(new ShortUrlResponse(
            ServletUriComponentsBuilder.fromUriString(this.getCurrentBasePath()).pathSegment(key).toUriString()
        ));
    }

    @GetMapping("/{shortenKey}")
    public ModelAndView redirect(HttpServletRequest request, @PathVariable("shortenKey") String shortenKey){
        SourceUrlBaseDto sourceUrl = urlService.getSourceUrl(shortenKey).orElseThrow(
            () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "URL not found"));

        request.setAttribute(View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.resolve(sourceUrl.getRedirectType()));

        return new ModelAndView("redirect:" + ServletUriComponentsBuilder.fromUriString(sourceUrl.getUrl()).toUriString());
    }

//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public ResponseEntity<ErrorResponse> handleError(HttpServletRequest req, Exception e) {
//        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNPROCESSABLE_ENTITY);
//    }

}
