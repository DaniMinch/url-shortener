package com.daniminch.ftry.urlshortener.shortening.dtos;

public class ShortUrlResponse {

    private String shortUrl;


    public ShortUrlResponse() {
    }

    public ShortUrlResponse(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }
}
