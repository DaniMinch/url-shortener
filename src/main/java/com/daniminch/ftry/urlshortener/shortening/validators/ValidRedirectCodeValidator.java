package com.daniminch.ftry.urlshortener.shortening.validators;

import org.springframework.http.HttpStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidRedirectCodeValidator implements ConstraintValidator<ValidRedirectCode, Integer> {

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return (value==HttpStatus.FOUND.value()) || (value==HttpStatus.MOVED_PERMANENTLY.value());
    }

}
