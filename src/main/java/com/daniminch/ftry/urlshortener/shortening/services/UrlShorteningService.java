package com.daniminch.ftry.urlshortener.shortening.services;

import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlRequest;

import java.util.Optional;

public interface UrlShorteningService {

    /**
     * Saves given source URL DTO and returns its shorten key

     * @param sourceRequest request object containing source URL
     * @return short code of URL
     */
    String saveAndGetKey(SourceUrlRequest sourceRequest);

    /**
     * Function tries to find original URL by short key
     *
     * @param shortenKey key in which URL id is encoded
     * @return Initial URL with all saved parameters
     */
    Optional<SourceUrlRequest> getSourceUrl(String shortenKey);
}
