package com.daniminch.ftry.urlshortener.shortening.daos;

import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UrlRepository extends MongoRepository<SourceUrlDocument, String> {

    SourceUrlDocument findByKey(long key);

}
