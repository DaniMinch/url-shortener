package com.daniminch.ftry.urlshortener.shortening.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


/**
 * Annotation is required to validate input of Redirect status codes
 */
@Documented
@Constraint(validatedBy = ValidRedirectCodeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ValidRedirectCode {

    String message() default "{val.err.not.part.of.enum}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}