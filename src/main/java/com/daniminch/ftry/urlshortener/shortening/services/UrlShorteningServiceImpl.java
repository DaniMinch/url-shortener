package com.daniminch.ftry.urlshortener.shortening.services;

import com.daniminch.ftry.urlshortener.shortening.daos.UrlRepository;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlDocument;
import com.daniminch.ftry.urlshortener.shortening.dtos.SourceUrlRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UrlShorteningServiceImpl implements UrlShorteningService {

    @Autowired
    private IdDispenserService idDispenser;

    @Autowired
    private IdConverterService idConverterService;

    @Autowired
    private UrlRepository repo;

    @Override
    public String saveAndGetKey(SourceUrlRequest sourceRequest) {

        // transform Request to Document
        SourceUrlDocument document = new SourceUrlDocument(sourceRequest);
        document.setKey(idDispenser.getNextID());

        // And save it
        document = repo.save(document);

        return idConverterService.encode(document.getKey());
    }

    @Override
    public Optional<SourceUrlRequest> getSourceUrl(String shortenKey) {

        SourceUrlDocument document = repo.findByKey(idConverterService.decode(shortenKey));

        if (document == null) {
            return Optional.empty();
        }

        return Optional.of(new SourceUrlRequest(document));
    }

}
