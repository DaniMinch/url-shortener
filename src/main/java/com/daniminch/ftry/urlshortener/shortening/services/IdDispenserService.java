package com.daniminch.ftry.urlshortener.shortening.services;

/**
 * Interface for getting unique ID
 */

public interface IdDispenserService {

    long getNextID();
}
