package com.daniminch.ftry.urlshortener.shortening.dtos;


import com.daniminch.ftry.urlshortener.shortening.validators.ValidRedirectCode;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

/**
 * Class is created to encapsulate members needed for all DTO
 * required for Source URL
 */
public class SourceUrlBaseDto {

    @NotBlank(message = "{val.err.source.url.is.blank}")
    @URL
    protected String url;

    @ValidRedirectCode
    protected int redirectType;

    public SourceUrlBaseDto() {}

    public SourceUrlBaseDto(SourceUrlBaseDto base) {
        this.setUrl(base.url);
        this.setRedirectType(base.redirectType);
    }

    public String getUrl() {
        return url;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

}
