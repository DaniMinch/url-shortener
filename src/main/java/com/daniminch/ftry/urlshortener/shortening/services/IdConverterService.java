package com.daniminch.ftry.urlshortener.shortening.services;

/**
 * Interface for converter of Integer IDs to Base62
 */
public interface IdConverterService {

    String encode(long num);

    long decode(String shortenKey);
}
