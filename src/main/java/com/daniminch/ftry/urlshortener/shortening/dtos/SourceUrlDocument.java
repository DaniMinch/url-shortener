package com.daniminch.ftry.urlshortener.shortening.dtos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "urls")
public class SourceUrlDocument extends SourceUrlBaseDto {

    public SourceUrlDocument() {}

    public SourceUrlDocument(SourceUrlBaseDto base) {
        super(base);
    }

    @Id
    public String id;

    private long key;

    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "SourceUrlDocument{" +
                "id='" + id + '\'' +
                ", key=" + key +
                ", url='" + url + '\'' +
                ", redirectType=" + redirectType +
                '}';
    }
}
