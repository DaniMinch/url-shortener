package com.daniminch.ftry.urlshortener.shortening.services;

import com.daniminch.ftry.urlshortener.utilities.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Service;

@Service
public class SeqBasedIdDispenserServiceImpl implements IdDispenserService {


    @Transient
    public static final String URL_SEQUENCE_NAME = "users_sequence";

    @Autowired
    SequenceGeneratorService generator;

    @Override
    public long getNextID() {
        return generator.getSequenceNumber(URL_SEQUENCE_NAME);
    }
}
