package com.daniminch.ftry.urlshortener.shortening.dtos;

public class SourceUrlRequest extends SourceUrlBaseDto {

    public SourceUrlRequest(){
        this.redirectType = 302;
    }

    public SourceUrlRequest(SourceUrlBaseDto base) {
        super(base);
    }

}
